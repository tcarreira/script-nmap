#!/bin/bash

echo -e ">>>>>>>>>>>>>>>>>>>>\033[32;1mInformação do Sistema:\033[0m<<<<<<<<<<<<<<<<<<<<\n"
hostnamectl
echo "-------------------------------------------------------------"
echo -e "\n\tNumero de cores:\t"`grep -c 'processor' /proc/cpuinfo`
echo -e "\tModelo do processador:\t"`cat /proc/cpuinfo | sed '5!d' | cut -d: -f2`
echo -e "\n>>>>>>>>>>>>>>>>>>>>\033[32;1mFim da Informação do Sistema\033[0m<<<<<<<<<<<<<<<<<<<<\n"

echo -e '\033[32;1mEscolhe um dos numeros abaixo:\033[0m'
echo "-------------------------------------------------------------"

PS3='Escolhe uma opção: '
options=("Validar/Instalar Nmap" "Listar Endereços ips ativos" "Listar portas abertas" "Encerrar")
select opt in "${options[@]}"
do
    case $opt in
        "Validar/Instalar Nmap")
            echo "Escolheste opção 1"
    if ! command -v nmap >/dev/null 2>&1 

          then
            echo "Nmap está a ser instalado, aguarde..."  
            sudo apt install nmap -y
          else
            echo    "Instalado"

            exit 1
fi
            ;;
         "Listar Endereços ips ativos")
            echo "Escolheste opção 2"
            nmap -sn 192.168.1.0/24
            ;;
        "Listar portas abertas")
            echo "Escolheste a opção 3"
            nmap --top-ports 20 192.168.1.0/24
            ;;
        "Encerrar")
            echo "Programa terminado"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
